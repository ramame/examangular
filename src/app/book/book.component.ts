import { Component, OnInit, Input} from '@angular/core';
import { BooksComponent } from '../books/books.component';
import { BooksService } from '../books.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  @Input() data:any;
  done:boolean;
write;
name;
show2;
showForm:boolean;
tempname;
key;
tempauth;
show5;
  constructor(private bookss:BooksService) { }
  showAuth(){
    this.show2 = true;
  }
  Notshow(){
    this.show2 = false;
  }
  
  save(){
   this.bookss.updateBook(this.key,this.name,this.write,this.done);
  }
  
  cancel(){
    console.log(this.show5)
    this.show5 = false;
    console.log(this.show5)
   this.name=this.tempname;
   this.write=this.tempauth;
  }

  seeUpdate(){
    this.show5 = true;
    this.tempname=this.name;
    this.tempauth=this.write;
    
  }
  ngOnInit() {
    this.showForm=false;
    this.show2=false
this.write = this.data.write;
this.name = this.data.text;
this.key = this.data.$key;
this.done = this.data.done;
  }

}
