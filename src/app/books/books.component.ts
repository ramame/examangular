import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router, ActivatedRoute } from '@angular/router';
import { BooksService } from '../books.service';
@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  bookname;
  writeby;

names =[];
books =[];
  constructor(private auth:AuthService, private db:AngularFireDatabase, private bookss:BooksService) { }

 addBook(){
   this.bookss.addBook(this.bookname,this.writeby)
 }







  ngOnInit() {

    this.auth.user.subscribe(user => {
      if(!user) return;
      this.db.list('/users/'+user.uid).snapshotChanges().subscribe(
        nicks =>{ console.log(nicks)                      
this.names = [];            
nicks.forEach(                   
nick => {                          
let y = nick.payload.toJSON();               
this.names.push(y); }) 
})})

this.auth.user.subscribe(user => {
  if(!user) return;
  this.db.list('/users/'+user.uid+'/books').snapshotChanges().subscribe(
    books =>{                   //this.todos זה זה מלמעלה
                                     //סתם todos יהיה זה מתוך הדטה בייס
    this.books = [];            //נאפס את כל הטודוס
    books.forEach(                   
    book => {                          // לכל אחד מהמשתנים במערך נקרא באופן זמני טודו
    let y = book.payload.toJSON();     //y is a  temp variable,  we fill him (y) with payload us json.
                                         //payload came us string so we need change it to json
    y["$key"] = book.key;              //אני מוסיף לכל טודו עוד תכונה שיקראו לה דולר קיי ובתוכה יהיה שם המפתח
                                       //key מילה שמורה
    this.books.push(y);                //מכניס את האיבר שיצרתי בתוך טודוס שלנו

  }

  ) 
}



    )  
})

}

}
