import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth'
import {Observable} from 'rxjs'
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
@Injectable({
  providedIn: 'root'
})
export class AuthService {



  
  user:Observable<firebase.User>;
  constructor(private fireBaseAuth:AngularFireAuth, 
    private db:AngularFireDatabase) { 
    this.user = fireBaseAuth.authState;
  }




  signup(email:string,password:string){
    return this.fireBaseAuth.auth.createUserWithEmailAndPassword(email,password) ;
  }

  addUser(user,nickname:string){
    let uid = user.uid;                      
    let ref = this.db.database.ref('/');
    ref.child('users').child(uid).push({'nickname':nickname});
                                                                                                    
      }
      updateUser(user,name:string){
        user.updateProfile({displayName:name,photoURL:''});
      }
      logout(){
        return this.fireBaseAuth.auth.signOut();
      }
login(email:string,password:string){
  return this.fireBaseAuth.auth.signInWithEmailAndPassword(email,password)
}
  
}
