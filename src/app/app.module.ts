import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { FormsModule } from '@angular/forms';
import { Routes,RouterModule } from '@angular/router';
import {MatInputModule} from '@angular/material';
import {environment} from '../environments/environment';
import { LoginComponent } from './login/login.component';
import { NavComponent } from './nav/nav.component';
import { BooksComponent } from './books/books.component';
import { BookComponent } from './book/book.component';
import { RegisterComponent } from './register/register.component';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavComponent,
    BooksComponent,
    BookComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MatCardModule,
    MatCheckboxModule,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
   RouterModule.forRoot([
    
     {path:'login', component:LoginComponent},
     {path:'register', component:RegisterComponent},
     {path:'books', component:BooksComponent},
     {path: 'books/:id', component: BooksComponent },
     {path:'**', component:BooksComponent}
   ])
 ],
  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
