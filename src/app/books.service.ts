import { Injectable } from '@angular/core';
import { AngularFireDatabase, snapshotChanges } from '@angular/fire/database';
import { AuthService } from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class BooksService {
  addBook(name,write){
    this.Auth.user.subscribe(value=>{
      let ref = this.db.database.ref('/');
      ref.child('users').child(value.uid).child('books').push({'text':name, 'write':write, 'done':false}); 
    })
  }
  updateBook(key,name:string,write:string,done:boolean){
    this.Auth.user.subscribe(value=>{
      let ref = this.db.database.ref('/');
      this.db.list('/users/'+value.uid+'/books/').update(key, {'text':name,'write':write, 'done':done})
    })
  }
  
  constructor(private db:AngularFireDatabase, private Auth:AuthService) { }
}
