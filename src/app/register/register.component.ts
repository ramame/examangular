import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
email='';
password='';
password2=''
nick ='';
name = '';
show1=false;
show2=false;
show3=false;
show4=false;
clicked1:boolean;
code = '';
msg = '';
refil = false;
  constructor(private auth:AuthService, private router:Router) { }

    signup(){
      if(this.name == ''){
        this.show1 = true
      }
      else{
        this.show1 = false
      }
      if(this.nick == ''){
        this.show2 = true
      }
      else{
        this.show2 = false
      }
      if(this.email == ''){
        this.show3 = true
      }
      else{
        this.show3 = false
      }
      if(this.password == ''){
        this.show4 = true
      }
      else{
        this.show4 = false;
      }
      if(this.password2==this.password){
      this.refil =false;
      
      if(this.show1== false&&this.show2 == false&&this.show3 == false&&this.show4 == false){
      this.auth.signup(this.email,this.password).then(value => {
        this.auth.updateUser(value.user,this.name);
        this.auth.addUser(value.user,this.nick);
      }).then(value =>{
        this.router.navigate(['books']);}).catch(err => {
        this.code=err.code
        this.msg=err.message
        
      })
   
      
    }}
    else{
      this.refil=true
    }
   
  }

  ngOnInit() {
    this.clicked1 = false;
  }

}
